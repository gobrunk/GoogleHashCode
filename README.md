Some useful things to know in order to have a good git repository.

**Commit messages should be like this** :   
"FEATURE: message"   
  As an example : git commit -m "PARSER: Updating the parser class."  

**Always do this when you want to push** :   
git pull --rebase   
git push   

**In case that there is some merge conflicts** :   
Correct them   
git add -u   
git rebase --continue   

**Always do this to create a new branch** :   
git checkout -b login_x/feature_name    

**Always proceed like this to merge a branch** :   
git checkout login_x/feature_name (The aim is to be on your branch)   
git rebase master   
git checkout master   
git merge login_x/feature_name

If we proceed like this, we should not have any problem on our git.

Best regards, and hope we won't forget to change this at the end.

Gobrunk


