#!/usr/bin/python3

import sys

output = "out.txt"
r,c,f,n,b,t =0,0,0,0,0,0
rides=[]
cars=[]
cars_cur=[]
def init_cars():
  global f
  for i in range(f):
    cars.append([0])
    cars_cur.append([False,0])


def parse():
  global r,c,f,n,b,t
  f = open(sys.argv[1], "r")
  i = -1
  for line in f:
    l = [int(a) for a in line[:-1].split()]
    if i == -1:
      r,c,f,n,b,t = l
    else:
      l.append(i)
      rides.append(l)
    i+=1;

def assign_to_free(index_v,step):
    global cars_cur,cars,rides
    rtd = []
    for i in range(len(rides)):
        if(rides[i][4] >= step):
            rtd = rides.pop(i)
            break
    if(rtd == []):
        return
    cars_cur[index_v][0] = True
    cars_cur[index_v][1] = step + abs(rtd[0] - rtd[2]) + abs(rtd[1] - rtd[3])
    cars[index_v].append(rtd[6])

def basic():
  a = 0
  for i in range(len(rides)):
    cars[a%len(cars)].append(i)
    a +=1
  return 0

def translate_out():
  f = open(output, "w")
  for car in cars:
    car[0] = len(car) -1
    for i in range(len(car)-1):
      print(car[i],file=f,end=' ')
    print(car[len(car)-1],file=f)


def parcours():
    global cars_cur,cars,rides
    step = 0
    while(len(rides)!= 0):
        for i in range(len(rides)):
            if((step + abs(rides[i][0] - rides[i][2]) + abs(rides[i][1] - rides[i][3])) > rides[i][5]):
                rides.pop(i)
        for i in range(f):
            if(cars_cur[i][0] == False):
                assign_to_free(i,step)
        for i in range(f):
            if(cars_cur[i][0] == True and cars_cur[i][1] <= step):
                cars_cur[i][0] = False
        step += 1

######################  main #################
parse()
rides.sort(key=lambda x:x[4])
init_cars()

parcours()

#MODIFY cars
#

translate_out()
